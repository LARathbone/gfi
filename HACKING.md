# gfi - Hacking Guide

## Introduction

Welcome! Like all of my projects, I aim to make gfi as hacker-friendly as
possible, especially for newcomers.

No suggestion or question is dumb, and everyone who puts forth a genuine effort
will be treated with utmost respect within this project.

I adopt fully the GNOME Code of Conduct, as well as the common-sense principle
of "Be Excellent to Each Other."

## Coding Style

We all have our relgious beliefs when it comes to coding styles. I am not
uber-pedantic when it comes to these things, but I do believe that consistency
within a codebase is important. To that end, here are some basic principles
that apply:

* To the extent possible, let's keep our user interface defined in .ui files,
  and reserve our C code for callbacks and things that C is actually good for,
  like, you know, programming!

* Tabs, not spaces. I prefer tab-shifts and shift-widths of 4, but that's the
  beauty of tabs! You can define them to be whatever you want them to be!

* When it comes to function definitions, I prefer the usual GNOME/GTK style,
  which is basically K&R style except with the return type on a separate line
  from the function name and parameters.

* When it comes to if, etc. statements, I'm not super picky. I just want the
  code to be as readable as possible.

* Keep a maximum of approximately 80 characters per line, please. I don't mind
  going a bit over, but I don't have the best eyesight and tend to look at code
  in vim with the source file in one column and the header in another, so keeping
  things at 80 characters keeps things relatively readable in 2-column displays
  without ugly wrapping.

* Absolutely no mid-block declarations.

## The Fine Print

Copyright © 2021 Logan Rathbone <poprocks@gmail.com>

See README.md and COPYING for licensing details.
