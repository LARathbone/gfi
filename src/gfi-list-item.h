// vim: colorcolumn=80 ts=4 sw=4

#pragma once 

#include <gtk/gtk.h>

#include "common.h"
#include "gfi-list-entry.h"

#define GFI_TYPE_LIST_ITEM gfi_list_item_get_type()
G_DECLARE_FINAL_TYPE (GfiListItem, gfi_list_item, GFI, LIST_ITEM,
		GtkWidget)

/* Method declarations */

GtkWidget *		gfi_list_item_new (void);
void			gfi_list_item_set_entry (GfiListItem *self, GfiListEntry *entry);
GfiListEntry *	gfi_list_item_get_entry (GfiListItem *self);

void			gfi_list_item_set_label (GfiListItem *self, const char *text);
void			gfi_list_item_set_icon_name (GfiListItem *self,
		const char *icon_name);
void			gfi_list_item_set_app_desc (GfiListItem *self, const char *text);
