// vim: colorcolumn=80 ts=4 sw=4 linebreak breakindent breakindentopt=shift\:4

#pragma once 

#include "common.h"

#define GFI_TYPE_LIST_ENTRY (gfi_list_entry_get_type ())
G_DECLARE_FINAL_TYPE (GfiListEntry, gfi_list_entry, GFI, LIST_ENTRY, GObject)

/* Method declarations */

GfiListEntry *		gfi_list_entry_new (GfiListWidgetType type,
		FlatpakInstalledRef *inst_ref);
void				gfi_list_entry_set_list_type (GfiListEntry *self, GfiListWidgetType type);
GfiListWidgetType	gfi_list_entry_get_list_type (GfiListEntry *self);
void				gfi_list_entry_set_inst_ref (GfiListEntry *self, FlatpakInstalledRef *inst_ref);
FlatpakInstalledRef *gfi_list_entry_get_inst_ref (GfiListEntry *self);
const char *		gfi_list_entry_get_app_xdg_name (GfiListEntry *self);
const char *		gfi_list_entry_get_app_pretty_name (GfiListEntry *self);
const char *		gfi_list_entry_get_app_desc (GfiListEntry *self);
void				gfi_list_entry_set_app_latest_version (GfiListEntry *self, const char *latest_version);
const char *		gfi_list_entry_get_app_latest_version (GfiListEntry *self);
void				gfi_list_entry_create_app_info_dialog (GfiListEntry *entry,
		GtkWindow *parent);
