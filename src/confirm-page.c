// vim: colorcolumn=80 ts=4 sw=4

#include "confirm-page.h"

static GtkWidget *confirm_text_view;
static GtkWidget *confirm_heading;
static GtkWidget *confirm_text;
static GtkWidget *confirm_expander;
static GtkWidget *confirm_expander_label;

static void
populate_buffer (GfiProgress *progress)
{
	GtkTextBuffer *buffer;
	GfiProgressActionType action_type;
	GSList *deplist;
	GtkTextIter iter;
	g_autofree char *total_dl_size = NULL;
	g_autofree char *total_dl_size_fmt = NULL;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW(confirm_text_view));
	action_type = gfi_progress_get_action_type (progress);
	deplist = gfi_progress_get_deplist (progress);
	gtk_text_buffer_get_start_iter (buffer, &iter);

	switch (action_type)
	{
		case GFI_PROGRESS_ACTION_TYPE_INSTALL:
			/* Insert a heading. */
			gtk_text_buffer_insert_markup (buffer,
					&iter,
					"<b>Package Name\tDownload Size</b>\n",
					-1);

			/* Populate data. */
			while (deplist)
			{
				GfiProgressData *prog_dat;
				g_autofree char *text = NULL, *size = NULL;

				prog_dat = deplist->data;

				size = g_format_size (prog_dat->dl_size);

				text = g_strdup_printf ("%s\t%s\n",
						prog_dat->name,
						size);

				gtk_text_buffer_insert_markup (buffer, &iter, text, -1);

				deplist = deplist->next;
			}

			/* Summarize with total dl size. */
			total_dl_size =
				g_format_size (gfi_progress_get_total_download_size (progress));

			total_dl_size_fmt = g_strdup_printf (
					"\n<b>Total download size:  %s</b>",
					total_dl_size);

			gtk_text_buffer_insert_markup (buffer, &iter, total_dl_size_fmt, -1);
			break;

		case GFI_PROGRESS_ACTION_TYPE_REMOVE:
			/* Insert a heading. */
			gtk_text_buffer_insert_markup (buffer,
					&iter,
					"<b>Packages targetted for removal:</b>\n",
					-1);

			/* Populate data. */
			while (deplist)
			{
				GfiProgressData *prog_dat;
				g_autofree char *text = NULL;

				prog_dat = deplist->data;
				text = g_strdup_printf ("%s\n", prog_dat->name);
				gtk_text_buffer_insert_markup (buffer, &iter, text, -1);

				deplist = deplist->next;
			}
			break;

		case GFI_PROGRESS_ACTION_TYPE_UPGRADE:
			/* Insert a heading. */
			gtk_text_buffer_insert_markup (buffer,
					&iter,
					"<b>Packages targetted for upgrade:</b>\n",
					-1);

			/* Populate data. 
			 * TODO - show version numbers of updates */
			while (deplist)
			{
				GfiProgressData *prog_dat;
				g_autofree char *text = NULL;

				prog_dat = deplist->data;
				text = g_strdup_printf ("%s\n", prog_dat->name);
				gtk_text_buffer_insert_markup (buffer, &iter, text, -1);

				deplist = deplist->next;
			}
			break;

		default:
			g_error ("%s: Programmer error: GfiProgressActionType has not "
					"been set properly. This should not happen. Abort.",
					__func__);
			break;
	}
}

static void
populate_data (GfiProgress *progress)
{
	GfiProgressActionType action_type;
	g_autofree char *heading_text;
	g_autofree char *confirm_blurb;

	action_type = gfi_progress_get_action_type (progress);

	switch (action_type)
	{
		case GFI_PROGRESS_ACTION_TYPE_INSTALL:
		{
			g_autofree char *dl_size;

			heading_text = g_strdup_printf (
					"Proceed with installation of: \"%s\"?",
					gfi_progress_get_appname (progress));

			dl_size = g_format_size (
					gfi_progress_get_total_download_size (progress));

			confirm_blurb = g_strdup_printf (
					"Any required dependencies will be "
					"downloaded and installed automatically.\n\n"
					"Total download size may be up to:  %s",
					dl_size);
		}
			break;

		case GFI_PROGRESS_ACTION_TYPE_REMOVE:
			heading_text = g_strdup_printf (
					"Proceed with removal of: \"%s\"?",
					gfi_progress_get_appname (progress));

			confirm_blurb = g_strdup_printf (
					/* Translators: the string here corresponds with the label
					 * of the expander to view advanced details of the transaction.
					 */
					"Other related packages may also be removed as part "
					"of this operation. Click \"%s\" for more information.",
					gtk_label_get_text (GTK_LABEL(confirm_expander_label)));
			break;

		case GFI_PROGRESS_ACTION_TYPE_UPGRADE:
			heading_text = g_strdup_printf (
					"Proceed with upgrade of: \"%s\"?",
					gfi_progress_get_appname (progress));

			confirm_blurb = g_strdup_printf (
					/* Translators: the string here corresponds with the label
					 * of the expander to view advanced details of the transaction.
					 */
					"Other related packages may also be upgraded as part "
					"of this operation. Click \"%s\" for more information.",
					gtk_label_get_text (GTK_LABEL(confirm_expander_label)));
			break;

		default:
			g_error ("%s: Programmer error: GfiProgressActionType has not "
					"been set. This should not happen. Abort.", __func__);
			break;
	}

	set_heading_text (GTK_LABEL(confirm_heading), heading_text);
	gtk_label_set_markup (GTK_LABEL(confirm_text), confirm_blurb);

	/* Fill out 'details' textview */
	populate_buffer (progress);
}

static void
setup_tabs (void)
{
	PangoTabArray *tabs;

	tabs = pango_tab_array_new (2, TRUE);
	pango_tab_array_set_tab (tabs, 0, PANGO_TAB_LEFT, 0);
	pango_tab_array_set_tab (tabs, 1, PANGO_TAB_LEFT, 400);
	gtk_text_view_set_tabs (GTK_TEXT_VIEW(confirm_text_view), tabs);
	pango_tab_array_free (tabs);
}

/* PUBLIC FUNCTIONS */

void
setup_confirm_page (GtkBuilder *builder, GfiProgress *progress)
{
	GET_WIDGET (builder, confirm_text_view);
	GET_WIDGET (builder, confirm_heading);
	GET_WIDGET (builder, confirm_text);
	GET_WIDGET (builder, confirm_expander_label);
	GET_WIDGET (builder, confirm_expander);

	gtk_widget_set_visible (confirm_expander, TRUE);
	setup_tabs ();
	populate_data (progress);
}
