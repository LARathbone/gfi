// vim: colorcolumn=80 ts=4 sw=4

#pragma once

#include <gtk/gtk.h>
#include <flatpak.h>

#include "common.h"
#include "gfi-list-item.h"

G_BEGIN_DECLS

#define GFI_TYPE_LIST_WIDGET (gfi_list_widget_get_type())
G_DECLARE_FINAL_TYPE (GfiListWidget, gfi_list_widget, GFI, LIST_WIDGET,
		GtkWidget)

/* Method declarations */

GtkWidget *		gfi_list_widget_new (GfiListWidgetType type);
void			gfi_list_widget_refresh (GfiListWidget *self);

G_END_DECLS
