// vim: colorcolumn=80 ts=4 sw=4

#pragma once

#include "common.h"
#include "gfi-progress.h"

void		setup_success_page (GtkBuilder *builder, GfiProgress *progress);
