// vim: colorcolumn=80 ts=4 sw=4

#pragma once 

#include <gtk/gtk.h>
#include <flatpak.h>
#include <appstream-glib.h>

/* MACROS */

/* GET_WIDGET
 *
 * Included because I'm sick of rewriting this again and again.
 *
 * This allows you to run GET_WIDGET(builder, widget) and will automatically
 * fetch "widget" (with quotation marks) from your builder object. In order
 * for this macro to work, your variable name (eg, widget, without quotes)
 * must match the ID name in your builder object (with quotes), which is
 * probably a good practice anyway.
 */

#define GET_WIDGET(B, W) \
	W = GTK_WIDGET(gtk_builder_get_object (B, #W));

#define IN_MAIN_CONTEXT \
	g_main_context_is_owner (g_main_context_default ())

/* Shorthand to get parent window of a GtkWidget, handling all casts for you. */
#define PARENT_WINDOW(W) \
	GTK_WINDOW(gtk_widget_get_native (GTK_WIDGET(W)))

/* FIXME - any point in this? FlatpakRefKind enum already exists. */
typedef enum
{
	KEYFILE_NAME_APPLICATION,
	KEYFILE_NAME_REFERENCE,
	KEYFILE_NAME_FLATPAKREF
} KeyfileNameType;

#define GFI_TYPE_LIST_WIDGET_TYPE (gfi_list_widget_type_get_type ())
GType gfi_list_widget_type_get_type (void);

typedef enum
{
	GFI_LIST_WIDGET_TYPE_REMOVE,
	GFI_LIST_WIDGET_TYPE_UPGRADE
} GfiListWidgetType;

/* FUNCTIONS */

/* helper function to set heading text based on default markup
 */
void	set_heading_text (GtkLabel *label, const char *text);

/* helper function to get an app or ref name from a flatpak keyfile
 * transfer: FULL
 */
char *	get_name_from_keyfile (GKeyFile *keyfile, KeyfileNameType type);

/* helper function to get an app description from a .desktop file
 * transfer: FULL
 */
char *	app_desc_from_xdg_id (const char *xdg_id);

/* helper function to get an app name from a .desktop file
 * transfer: FULL
 */
char *	app_name_from_xdg_id (const char *xdg_id);

/* helper function to get an exec cmdline string from a .desktop file
 * transfer: FULL
 */
char *	exec_cmdline_from_xdg_id (const char *xdg_id);

gboolean startup_notify_from_xdg_id (const char *xdg_id);
gboolean needs_terminal_from_xdg_id (const char *xdg_id);

/* Some non-blocking flatpak helper functions: */

void	newest_version_of_app_from_installed_ref_async (FlatpakInstalledRef *inst_ref,
			GCancellable *cancellable, GAsyncReadyCallback callback,
			gpointer user_data);
char *	newest_version_of_app_from_installed_ref_finish (FlatpakInstalledRef *inst_ref,
			GAsyncResult *result);

void	list_installed_refs_for_update_async (FlatpakInstallation *installation,
			GCancellable *cancellable, GAsyncReadyCallback callback,
			gpointer user_data);
/* [element-type FlatpakInstalledRef] */
GPtrArray *	list_installed_refs_for_update_finish (FlatpakInstallation *installation,
			GAsyncResult *result);

void	list_installed_refs_async (FlatpakInstallation *installation,
			GCancellable *cancellable, GAsyncReadyCallback callback,
			gpointer user_data);
/* [element-type FlatpakInstalledRef] */
GPtrArray *	list_installed_refs_finish (FlatpakInstallation *installation,
			GAsyncResult *result);

/* DEBUG */
void	debug_print_keyfile (GKeyFile *keyfile);
