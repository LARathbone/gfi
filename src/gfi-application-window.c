// vim: ts=4 sw=4 colorcolumn=80

#include "gfi-application-window.h"

#define UI_RESOURCE RESOURCE_PREFIX "gfi-application-window.ui"

struct _GfiApplicationWindow
{
	GtkApplicationWindow parent_instance;

	/* From template: */
#if 0
:r!for i in `cat ../ui/gfi-application-window.ui |grep -i 'object.*id=' |sed -e 's,^\s*,,g' |sed -e 's,.*id=",,' |sed -e 's,".*,,'`; do echo 'GtkWidget *'${i}';'; done
#endif

	GtkWidget *headerbar;
	GtkWidget *refresh_button;
	GtkWidget *hamburger_menu_button;
	GtkWidget *stack;
	GtkWidget *remove_list_widget;
	GtkWidget *upgrade_list_widget;
};

G_DEFINE_TYPE (GfiApplicationWindow, gfi_application_window,
		GTK_TYPE_APPLICATION_WINDOW)

/* CALLBACKS */

static void
refresh_done_cb (GfiListWidget *gfi_list_widget,
		gpointer user_data)
{
	GfiApplicationWindow *self = GFI_APPLICATION_WINDOW(user_data);

	gtk_widget_set_sensitive (self->refresh_button, TRUE);
}

/* ACTIONS */

static void
about_acn (GtkWidget *widget,
		const char *action_name,
		GVariant *parameter)
{
	GfiApplicationWindow *self = GFI_APPLICATION_WINDOW(widget);

	const char *authors[] = {
		"Logan Rathbone",
		NULL
	};

	const char *documentation_credits[] = {
		"Logan Rathbone",
		NULL
	};

	const char *licence =
		"This program is free software; you can redistribute it and/or modify "
		"it under the terms of the GNU General Public License as published by "
		"the Free Software Foundation, version 2.\n\n"

	   "This program is distributed in the hope that it will be useful, "
	   "but WITHOUT ANY WARRANTY; without even the implied warranty of "
	   "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
	   "GNU General Public License for more details.\n\n"

	   "You should have received a copy of the GNU General Public License "
	   "along with this program; if not, write to the Free Software Foundation, Inc., "
	   "51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA";

	const char *copyright = "Copyright © 2021 Logan Rathbone";

	gtk_show_about_dialog (GTK_WINDOW(self),
	                       "authors", authors,
	                       "comments", "GTK Flatpak Installer",
	                       "copyright", copyright,
	                       "documenters", documentation_credits,
	                       "license", licence,
	                       "logo-icon-name", XDG_NAME,
	                       "program-name", PACKAGE_NAME,
	                       "title", "About",
	                       "translator-credits", "translator-credits",
	                       "version", PACKAGE_VERSION,
	                       "website", "https://gitlab.gnome.org/LARathbone/gfi",
	                       "website-label", "Website",
	                       "wrap-license", TRUE,
	                       NULL);
}

static void
refresh_acn (GtkWidget *widget,
		const char *action_name,
		GVariant *parameter)
{
	GfiApplicationWindow *self = GFI_APPLICATION_WINDOW(widget);

	gtk_widget_set_sensitive (self->refresh_button, FALSE);
	gfi_application_window_refresh (self);
}

/* CONSTRUCTORS AND DESTRUCTORS */

static void
gfi_application_window_init (GfiApplicationWindow *self)
{
	GtkWidget *widget = GTK_WIDGET(self);
	GtkStyleContext *context;
	GtkCssProvider *provider;

	/* Keep this at the top. */
	gtk_widget_init_template (widget);

	g_signal_connect (self->remove_list_widget, "refresh-done",
			G_CALLBACK (refresh_done_cb), self);

	g_signal_connect (self->upgrade_list_widget, "refresh-done",
			G_CALLBACK (refresh_done_cb), self);
}

static void
gfi_application_window_dispose (GObject *object)
{
	GfiApplicationWindow *self = GFI_APPLICATION_WINDOW(object);

	/* Chain up */
	G_OBJECT_CLASS(gfi_application_window_parent_class)->dispose(object);
}

static void
gfi_application_window_finalize (GObject *gobject)
{
	/* Chain up */
	G_OBJECT_CLASS(gfi_application_window_parent_class)->finalize(gobject);
}

static void
gfi_application_window_class_init (GfiApplicationWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	object_class->dispose = gfi_application_window_dispose;
	object_class->finalize = gfi_application_window_finalize;

	/* WIDGET TEMPLATE .UI */

	g_type_ensure (GFI_TYPE_LIST_WIDGET);

	gtk_widget_class_set_template_from_resource (widget_class, UI_RESOURCE);

#if 0
:r!for i in `cat ../ui/gfi-application-window.ui |grep -i 'object.*id=' |sed -e 's,^\s*,,g' |sed -e 's,.*id=",,' |sed -e 's,".*,,'`; do echo "gtk_widget_class_bind_template_child (widget_class, GfiApplicationWindow, ${i});"; done
#endif

	gtk_widget_class_bind_template_child (widget_class, GfiApplicationWindow, headerbar);
	gtk_widget_class_bind_template_child (widget_class, GfiApplicationWindow, refresh_button);
	gtk_widget_class_bind_template_child (widget_class, GfiApplicationWindow, hamburger_menu_button);
	gtk_widget_class_bind_template_child (widget_class, GfiApplicationWindow, stack);
	gtk_widget_class_bind_template_child (widget_class, GfiApplicationWindow, remove_list_widget);
	gtk_widget_class_bind_template_child (widget_class, GfiApplicationWindow, upgrade_list_widget);

	/* ACTIONS */

	gtk_widget_class_install_action (widget_class, "win.refresh",
			NULL,
			refresh_acn);

	gtk_widget_class_install_action (widget_class, "app.about",
			NULL,
			about_acn);
}

/* PUBLIC METHOD DEFINITIONS */

GtkWidget *
gfi_application_window_new (GtkApplication *app)
{
	return g_object_new (GFI_TYPE_APPLICATION_WINDOW,
			"application", app,
			NULL);
}

void
gfi_application_window_refresh (GfiApplicationWindow *self)
{
	g_return_if_fail (GFI_IS_APPLICATION_WINDOW (self));

	gfi_list_widget_refresh (GFI_LIST_WIDGET(self->remove_list_widget));
	gfi_list_widget_refresh (GFI_LIST_WIDGET(self->upgrade_list_widget));
}

GtkWidget *
gfi_application_window_get_list_widget (GfiApplicationWindow *self,
		GfiListWidgetType type)
{
	g_return_val_if_fail (GFI_IS_APPLICATION_WINDOW (self), NULL);

	switch (type)
	{
		case GFI_LIST_WIDGET_TYPE_REMOVE:
			return self->remove_list_widget;
			break;

		case GFI_LIST_WIDGET_TYPE_UPGRADE:
			return self->upgrade_list_widget;
			break;

		default:
			g_error ("%s: Programmer error - "
					"invalid GfiListWidgetType provided.",
					__func__);
			break;
	}
}
