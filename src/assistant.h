// vim: colorcolumn=80 ts=4 sw=4

#pragma once

#include "common.h"

#include "gfi-progress.h"
#include "confirm-page.h"
#include "progress-page.h"
#include "success-page.h"

GtkWidget *		gfi_assistant_new (GfiProgress *progress);
void			gfi_assistant_abort (void);
