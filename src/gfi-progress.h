// vim: colorcolumn=80 ts=4 sw=4

#pragma once

#include "common.h"
#include <flatpak.h>

G_BEGIN_DECLS

/* Type declaration */

#define GFI_TYPE_PROGRESS gfi_progress_get_type()
G_DECLARE_FINAL_TYPE(GfiProgress, gfi_progress, GFI, PROGRESS, GObject)

/* Public enums */

typedef enum {
	GFI_PROGRESS_TRANSACTION_TYPE_UNKNOWN,
	GFI_PROGRESS_TRANSACTION_TYPE_APP,
	GFI_PROGRESS_TRANSACTION_TYPE_DEP
} GfiProgressTransactionType;

typedef enum {
	GFI_PROGRESS_ACTION_TYPE_NONE,
	GFI_PROGRESS_ACTION_TYPE_INSTALL,
	GFI_PROGRESS_ACTION_TYPE_REMOVE,
	GFI_PROGRESS_ACTION_TYPE_UPGRADE
} GfiProgressActionType;

typedef struct GfiProgressData {
	char *name;
	guint64 dl_size;
} GfiProgressData;

/* Method declarations */

GfiProgress *	gfi_progress_new (void);
gboolean		gfi_progress_load_flatpakref (GfiProgress *self,
					GFile *flatpakref_file);

gboolean		gfi_progress_load_ref_str (GfiProgress *self, 
		const char *ref_str, GfiProgressActionType type);
gboolean		gfi_progress_load_ref (GfiProgress *self, FlatpakRef *ref,
		GfiProgressActionType type);
void			gfi_progress_start (GfiProgress *self);
void			gfi_progress_confirm (GfiProgress *self);
void			gfi_progress_proceed (GfiProgress *self);
const char *	gfi_progress_get_status (GfiProgress *self);
GfiProgressTransactionType
				gfi_progress_get_transaction_type (GfiProgress *self);
GfiProgressActionType
				gfi_progress_get_action_type (GfiProgress *self);
const char *	gfi_progress_get_transaction_name (GfiProgress *self);
const char *	gfi_progress_get_last_error_msg (GfiProgress *self);
guint64			gfi_progress_get_total_download_size (GfiProgress *self);
GSList *		gfi_progress_get_deplist (GfiProgress *self);
const char *	gfi_progress_get_appname (GfiProgress *self);
GCancellable *	gfi_progress_get_cancellable (GfiProgress *self);

/* GfiProgressData methods */

GfiProgressData *	gfi_progress_data_new (void);
void				gfi_progress_data_free (GfiProgressData *data);

G_END_DECLS
