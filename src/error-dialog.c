// vim: colorcolumn=80 ts=4 sw=4

#include "error-dialog.h"

#define UI_RESOURCE RESOURCE_PREFIX "error-dialog.ui"

static GtkWidget *error_dialog;

static void
response_cb (GtkDialog *dialog,
		int response_id,
		gpointer user_data)
{
	/* the only button is to close... */
	gtk_window_destroy (GTK_WINDOW(error_dialog));
}

GtkWidget *
gfi_error_dialog_new (GtkWindow *parent)
{
	g_autoptr(GtkBuilder) builder = NULL;

	g_return_val_if_fail (GTK_IS_WINDOW (parent), NULL);

	builder = gtk_builder_new_from_resource (UI_RESOURCE);

	GET_WIDGET (builder, error_dialog);

	/* setup signals */
	g_signal_connect (error_dialog, "response",
			G_CALLBACK(response_cb), NULL);

	gtk_window_set_transient_for (GTK_WINDOW(error_dialog), parent);
	gtk_window_set_modal (GTK_WINDOW(error_dialog), TRUE);

	return error_dialog;
}
