// vim: colorcolumn=80 ts=4 sw=4

#include "success-page.h"

static GtkWidget *success_heading;
static GtkWidget *success_text;


/* PUBLIC FUNCTIONS */

void
setup_success_page (GtkBuilder *builder, GfiProgress *progress)
{
	GfiProgressActionType action_type;

	g_return_if_fail (GFI_IS_PROGRESS (progress));

	GET_WIDGET (builder, success_heading);
	GET_WIDGET (builder, success_text);

	action_type = gfi_progress_get_action_type (progress);

	switch (action_type)
	{
		case GFI_PROGRESS_ACTION_TYPE_INSTALL:
			/* do nothing - defaults from the .ui file apply */
			break;

		case GFI_PROGRESS_ACTION_TYPE_REMOVE:
			set_heading_text (GTK_LABEL(success_heading),
					"Removal Complete");
			gtk_label_set_text (GTK_LABEL(success_text),
					"You may close this window now.");
			break;

		case GFI_PROGRESS_ACTION_TYPE_UPGRADE:
			set_heading_text (GTK_LABEL(success_heading),
					"Upgrade Complete");
			gtk_label_set_text (GTK_LABEL(success_text),
					"You may close this window now.");
			break;

		default:
			g_error ("%s: Programmer error: GfiProgressActionType has not "
					"been set. This should not happen. Abort.", __func__);
			break;
	}
}
