// vim: colorcolumn=80 ts=4 sw=4

#pragma once

#include <gtk/gtk.h>
#include "gfi-list-widget.h"

#define GFI_TYPE_APPLICATION_WINDOW (gfi_application_window_get_type ())
G_DECLARE_FINAL_TYPE (GfiApplicationWindow, gfi_application_window, GFI, APPLICATION_WINDOW,
				GtkApplicationWindow)

/* METHOD DECLARATIONS */

GtkWidget *	gfi_application_window_new (GtkApplication *app);
void		gfi_application_window_refresh (GfiApplicationWindow *self);
GtkWidget *	gfi_application_window_get_list_widget (GfiApplicationWindow *self,
		GfiListWidgetType type);
