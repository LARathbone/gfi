# vim: colorcolumn=80

PACKAGE_NAME = gfi
PACKAGE_VERSION = 0.3.0
XDG_NAME = org.gnome.gitlab.LARathbone.gfi
RESOURCE_PREFIX = /org/gnome/gitlab/LARathbone/gfi/

PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DATADIR ?= $(PREFIX)/share

DEP_CFLAGS = $(shell pkg-config --libs --cflags gtk4 flatpak appstream-glib)
GLIB_CFLAGS = -DG_LOG_DOMAIN=\"$(PACKAGE_NAME)\"
DEBUG ?= -g

CFLAGS ?= -Wall -Wextra -Wshadow -Wno-unused \
	  -Werror=implicit-function-declaration \
	  -Wdeclaration-after-statement \
	  -std=c11 \
	  -DPACKAGE_NAME=\"$(PACKAGE_NAME)\" \
	  -DPACKAGE_VERSION=\"$(PACKAGE_VERSION)\" \
	  -DXDG_NAME=\"$(XDG_NAME)\" \
	  -DRESOURCE_PREFIX=\"$(RESOURCE_PREFIX)\" \
	  $(DEBUG) \
	  $(DEP_CFLAGS) \
	  $(GLIB_CFLAGS)
