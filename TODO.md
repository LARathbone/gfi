# gfi TODO

**Last updated:  7 September 2021**

Note that this is alpha software and not yet ready for prime time. I am using
traditional definition of alpha to mean "not feature complete."

Here are some items that are currently considered to be blockers for reaching
beta level and beyond.

## Beta Blockers

* Preferences dialog

* Some management of remotes, even if rudimentary and user-level only

* Implement the meson build system

## Beyond Beta

* Currently only user-based flatpak management is supported. I am undecided
  whether I would like system-wide flatpak management to be supported by gfi.
  This is something that will be determined post-beta or even post-1.0.

## Known Issues

* Spamming the refresh button can cause the application to crash.
