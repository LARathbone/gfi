# gfi: GTK Flatpak Installer

## Overview

gfi is a simple Flatpak installer and manager, primarily built provide a
graphical interface for installing and managing Flatpak software from
repositories such as Flathub.

Please note that gfi is currently very much a work in progress, and is in alpha
state.  Use at your own risk, and only give gfi a try if you are interested in
being a member of the testing community, or if you would like to hack on the
project.

## Dependencies

* gtk >= 4.0.0
* glib >= 2.60
* flatpak >= 1.10.0
* appstream-glib >= 0.5.12                                                      

In order to compile, you will require a recent enough version of either the gcc
or clang C compiler to support C11. As of the time of writing, I have primarily
tested with gcc. The project seems to build find with clang using llvm 12.0.1,
but some spurious warnings about unused items are issued that are not caught by
-Wno-unused as they are with gcc.

GNU Make is also required.

You must, of course, have all development libraries installed to compile gfi.
Distributions that split development libraries from the runtime libraries in
order to save a few kilobytes often suffix the development packages with names
like "-dev" and "-devel".

## Compilation

Run `make` in the main directory.

You will then find the `gfi` binary in the `src` directory. A `debug.sh` shell
script is also included for testing purposes to assist in ensuring that runtime
debugging information is printed to the console.

If you would like to install gfi to your system, you may run `make install` as
root.

## Usage

After installing, run `gfi --help` for usage.

## The Fine Print

Copyright © 2021-2025 Logan Rathbone <poprocks@gmail.com>

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, version 2 ("GPLv2"). For greater certainty, unlike many other free
software projects, you may not redistribute and/or modify this program under a
later version of the GNU General Public License at your option.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA  02110-1301, USA.

See COPYING to review the full GPLv2 licence.
