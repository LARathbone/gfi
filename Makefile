# dependencies:
#
# gtk >= 4.0.0
# glib2 >= 2.60
# flatpak >= 1.10.0
# appstream-glib >= 0.5.12

include ./common.mk

SUBDIRS = ui src
BINARY = $(PACKAGE_NAME)

default:
	$(foreach subdir,$(SUBDIRS),$(MAKE) -C $(subdir);)

clean:
	$(foreach subdir,$(SUBDIRS),$(MAKE) -C $(subdir) clean;)

install:
	mkdir -p $(DESTDIR)$(BINDIR)
	install -m 755 src/gfi $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(DATADIR)/applications
	install -m 644 data/gfi.desktop \
		$(DESTDIR)$(DATADIR)/applications/$(XDG_NAME).desktop
	install -m 644 data/gfi-wizard.desktop \
		$(DESTDIR)$(DATADIR)/applications/$(XDG_NAME)_wizard.desktop
	mkdir -p $(DESTDIR)$(DATADIR)/icons/hicolor/scalable/apps/
	install -m 644 data/gfi.svg \
		$(DESTDIR)$(DATADIR)/icons/hicolor/scalable/apps/$(XDG_NAME).svg
	mkdir -p $(DESTDIR)$(DATADIR)/icons/hicolor/symbolic/apps/
	install -m 644 data/gfi-symbolic.svg \
		$(DESTDIR)$(DATADIR)/icons/hicolor/symbolic/apps/$(XDG_NAME)-symbolic.svg

uninstall:
	rm -f $(BINDIR)/gfi
	rm -f $(DATADIR)/applications/$(XDG_NAME).desktop
	rm -f $(DATADIR)/applications/$(XDG_NAME)_wizard.desktop
	rm -f $(DATADIR)/icons/hicolor/scalable/apps/$(XDG_NAME).svg
	rm -f $(DATADIR)/icons/hicolor/symbolic/apps/$(XDG_NAME)-symbolic.svg

.PHONY: default clean install


# vim: colorcolumn=80
